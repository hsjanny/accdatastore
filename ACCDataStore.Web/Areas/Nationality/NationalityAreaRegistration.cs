﻿using System.Web.Mvc;

namespace ACCDataStore.Web.Areas.Nationality
{
    public class NationalityAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Nationality";
            }
        }

        public override void 
            RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Nationality_default",
                "Nationality/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}